#!/bin/bash

arquivo=0

menu(){
	echo "1. Escolher arquivo zip existente"
	echo "2. Criar arquivo zip"
	echo "3. Sair"
}

menu_funcoes(){
	echo "1. Listar conteúdo"
	echo "2. Pré-visualizar arquivo compactado"
	echo "3. Adicionar arquivo"
	echo "4. Remover arquivo."
	echo "5. Extrair conteúdo total"
	echo "6. Extrair arquivos específicos"
	echo "7. Encerrar"
}

while true; do
	menu
        read -p "Opção: " opcao
	case opcao in
		1) read -p "Arquivo: " arquivozip
		        if [ -f "$arquivozip" ]; then
		                arquivo=$arquivozip
		                echo "Arquivo escolhido: $arquivozip"
                        else
		                echo "Arquivo inexistente"
	                fi
			;;
		2) read -p "Nome do arquivo: " arquivozip
		        if [ ! -f "$arquivozip" ]; then
				touch "$arquivozip"
				arquivo=$arquivozip
				echo "Arquivo zip criado: $arquivozip"
		        fi
			;;
		3) echo "Encerrando"
		   exit 0;;

		*) echo "Opção inválida"

	esac
done


while true; do
	menu_funcoes
	read -p "Opção: " opcao
	case opcao in
		1) unzip -1 "$arquivo"
		   ;;
		2) read -p "Digite o nome do arquivo a ser pré-visualizado: " nome
			unzip -p "$arquivozip" "$nome"
		   ;;
		3) read -p "Digite onde o arquivo será adicionado" novo
			zip -r "$arquivozip" "$novo"
		   ;;
		4) read -p "Digite o nome do arquivo a ser removido: " nome
			zip -d "$arquivozip" "$nome"
		   ;;
		5) unzip "$arquivozip"
		   ;;
		6) read -p "Digite o nome do arquivo a ser extraído: " nome
			unzip "$arquivozip" "$nome"
		   ;;
		7) echo "Encerrando programa"
		   exit 0
		   ;;
		*) echo "Opção inválida"
	           ;;
	esac
done





