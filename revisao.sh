#!/bin/bash

while true; do
    echo """
    1. Enviar Ping para ip ou site
    2. Listar usuários logados
    3. Exibir uso de memória e disco da máquina
    4. Sair
    """

    read -p "Opção: " opcao

    if [ "$opcao" -eq "1" ]; then
        read -p "HOST: " host
        ping -c 4 "$host"

    elif [ "$opcao" -eq "2" ]; then
        who

    elif [ "$opcao" -eq "3" ]; then
        free -h
        df -h

    elif [ "$opcao" -eq "4" ]; then
        echo "Saindo do script."
        break

    else
        echo "Digite uma opção válida."
    fi
done
