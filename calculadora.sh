#!/bin/bash

function menu(){
	echo "1. Adicionar"
	echo "2. Subtrair"
	echo "3. Multiplicar"
        echo "4. Dividir"
        echo "Z. Encerrar"	
}

valor=0

while true; do  
        menu
        read -p "Operação: " operacao

        case $operacao in
        1) read -p "Valor: " novo
		valor=$((valor + novo))
                echo "$valor";;
	2) read -p "Valor: " novo
		valor=$((valor - novo))
                echo "$valor";;
        3) read -p "Valor: " novo
		valor=$((valor * novo))
                echo "$valor";;
        4) read -p "Valor: " novo
		valor=$((valor / novo))
                echo "$valor";;
        z) echo "Encerrando"
         	exit 0;;
	*) echo "Opção inválida"
		
        esac
done
